﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace chip8
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		private Emulator m_emulator;
		private Texture2D m_texture;

		private Keys[] m_keymap =
		{
			Keys.D1, Keys.D2, Keys.D3, Keys.D4,
			Keys.Q, Keys.W, Keys.E, Keys.R,
			Keys.A, Keys.S, Keys.D, Keys.F,
			Keys.Z, Keys.X, Keys.C, Keys.V
		};

		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			graphics.PreferredBackBufferWidth = 1280;
			graphics.PreferredBackBufferHeight = 640;
			graphics.SynchronizeWithVerticalRetrace = false;
			IsFixedTimeStep = true;
			TargetElapsedTime = TimeSpan.FromSeconds(1 / 120.0f);
			Content.RootDirectory = "Content";

		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			m_emulator = new Emulator();
			m_emulator.Init();
			m_emulator.LoadGame(@"C:\Users\Wolf\Documents\visual studio 2015\Projects\chip8\chip8\roms\pong.ch8");


			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);
			m_texture = new Texture2D(GraphicsDevice, m_emulator.Width, m_emulator.Height);
		}


		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// game-specific content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();
			//Console.WriteLine("FPS :" + 1 / gameTime.ElapsedGameTime.TotalSeconds);

			m_emulator.EmulateCycle();

			if (m_emulator.m_drawFlag)
				drawGraphics();

			m_emulator.SetKeys(m_keymap);

			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{

			//GraphicsDevice.Clear(Color.CornflowerBlue);
			spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null, null);
			spriteBatch.Draw(m_texture, new Rectangle(0, 0, 1280, 640), Color.White);
			spriteBatch.End();

			base.Draw(gameTime);
		}

		public void drawGraphics()
		{
			m_emulator.m_drawFlag = false;
			Color[] data = new Color[m_emulator.Width * m_emulator.Height];
			for (int y = 0; y < 32; ++y) {
				for (int x = 0; x < 64; ++x) {
					if (m_emulator.m_graphics[(y * 64) + x] == 0) {
						data[(y * 64) + x] = Color.Black;
					}
					else {
						data[(y * 64) + x] = Color.White;
					}
				}
			}
			m_texture.SetData(data);
		}

	}
}
