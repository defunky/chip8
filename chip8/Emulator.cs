﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace chip8
{
	class Emulator
	{

		private ushort m_opcode;
		byte[] m_memory;
		byte[] m_registers;

		private ushort m_indexRegister;
		private ushort m_programCounter;

		public byte[] m_chip8_fontset;

		public byte[] m_graphics;

		private byte m_delay_timer;
		private byte m_sound_timer;

		ushort[] m_stack;
		private ushort m_stackpointer;

		byte[] m_keypad;


		public bool m_drawFlag;

		private Random m_random;

		public int Width => 64;
		public int Height => 32;

		public Emulator()
		{
			m_graphics = new byte[Width * Height];
			m_memory = new byte[4096];
			m_registers = new byte[16];
			m_stack = new ushort[16];
			m_keypad = new byte[16];
			m_chip8_fontset = new byte[]
			{
				0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
				0x20, 0x60, 0x20, 0x20, 0x70, // 1
				0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
				0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
				0x90, 0x90, 0xF0, 0x10, 0x10, // 4
				0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
				0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
				0xF0, 0x10, 0x20, 0x40, 0x40, // 7
				0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
				0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
				0xF0, 0x90, 0xF0, 0x90, 0x90, // A
				0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
				0xF0, 0x80, 0x80, 0x80, 0xF0, // C
				0xE0, 0x90, 0x90, 0x90, 0xE0, // D
				0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
				0xF0, 0x80, 0xF0, 0x80, 0x80 // F
			};

			m_random = new Random();
		}

		public void Init()
		{
			m_programCounter = 0x200; //Expects application to be loaded here
			m_opcode = 0;
			m_indexRegister = 0;
			m_stackpointer = 0;

			Array.Clear(m_graphics, 0, m_graphics.Length); //Clear gfx
			Array.Clear(m_stack, 0, m_stack.Length); //Clear stack
			Array.Clear(m_registers, 0, m_registers.Length); //Clear registers
			Array.Clear(m_memory, 0, m_memory.Length); // clear memory

			for (int i = 0; i < 80; i++) {
				m_memory[i] = m_chip8_fontset[i];
			}

			m_delay_timer = 0;
			m_sound_timer = 0;
		}

		public void LoadGame(string game)
		{
			if (File.Exists(game)) {
				byte[] fileBytes = File.ReadAllBytes(game);
				for (int i = 0; i < fileBytes.Length; i++) {
					m_memory[i + 512] = fileBytes[i];
				}
			}
		}

		public void EmulateCycle()
		{
			//Note: Every instruction is 2 bytes long, so increment counter by 2 after each execution.
			//If the next opcode is to be skipped jump by 4.

			//Fetch opcode - opcode is two bytes long, so fetch first one in memory shift it and merge it with the next one.
			m_opcode = (ushort)(m_memory[m_programCounter] << 8 | m_memory[m_programCounter + 1]);

			//https://en.wikipedia.org/wiki/CHIP-8#Opcode_table
			//Decode opcode
			//NNN: Address
			//NN: 8-bit constant
			//N: 4-bit constant
			//X and Y: 4-bit register id
			//PC: Program counter
			//I: 16 bit register
			switch (m_opcode & 0xF000) //Read first 4 bits of opcode
			{
				case 0x0000:
					switch (m_opcode & 0x000F) {
						case 0x0000: // 00E0: Clears the screen. 
							Array.Clear(m_graphics, 0, m_graphics.Length);
							m_drawFlag = true;
							m_programCounter += 2;
							break;
						case 0x000E: // 00EE: Returns from a subroutine. 
							m_stackpointer--;
							m_programCounter = m_stack[m_stackpointer];
							m_programCounter += 2; //We add to go to next opcode or we end up with infinite loop 
							break;
						default: //Possibly 0NNN
							Debug.Assert(true, $"Opcode not defined - Opcode:{m_opcode}");
							break;
					}
					break;
				case 0x1000: // 1NNN: Jumps to address NNN. 
					m_programCounter = (ushort)(m_opcode & 0x0FFF);
					break;
				case 0x2000: // 2NNN: Calls subroutine at NNN. 
					m_stack[m_stackpointer] = m_programCounter;
					m_stackpointer++;
					m_programCounter = (ushort)(m_opcode & 0x0FFF);
					break;
				case 0x3000: // 3XNN: Skips the next instruction if VX equals NN. (Usually the next instruction is a jump to skip a code block) 
					if (m_registers[(m_opcode & 0x0F00) >> 8] == (m_opcode & 0x00FF))
						m_programCounter += 4;
					else
						m_programCounter += 2;
					break;
				case 0x4000: // 4XNN: Skips the next instruction if VX doesn't equal NN. (Usually the next instruction is a jump to skip a code block) 
					if (m_registers[(m_opcode & 0x0F00) >> 8] != (m_opcode & 0x00FF))
						m_programCounter += 4;
					else
						m_programCounter += 2;
					break;
				case 0x5000: // 5XY0: Skips the next instruction if VX equals VY. (Usually the next instruction is a jump to skip a code block) 
					if (m_registers[(m_opcode & 0x0F00) >> 8] == (m_opcode & 0x00F0))
						m_programCounter += 4;
					else
						m_programCounter += 2;
					break;
				case 0x6000: // 6XNN: Sets VX to NN.
					m_registers[(m_opcode & 0x0F00) >> 8] = (byte)(m_opcode & 0x00FF);
					m_programCounter += 2;
					break;
				case 0x7000: //7XNN: Adds NN to VX. (Carry flag is not changed) 
					m_registers[(m_opcode & 0x0F00) >> 8] += (byte)(m_opcode & 0x00FF);
					m_programCounter += 2;
					break;
				case 0x8000:
					switch (m_opcode & 0x000F) {
						case 0x0000: // 8XY0: Sets VX to the value of VY. 
							m_registers[(m_opcode & 0x0F00) >> 8] = m_registers[(m_opcode & 0x00F0) >> 4];
							m_programCounter += 2;
							break;
						case 0x0001: // 8XY1: Sets VX to VX or VY. (Bitwise OR operation) 
							m_registers[(m_opcode & 0x0F00) >> 8] |= m_registers[(m_opcode & 0x00F0) >> 4];
							m_programCounter += 2;
							break;
						case 0x0002: // 8XY2: Sets VX to VX and VY. (Bitwise AND operation) 
							m_registers[(m_opcode & 0x0F00) >> 8] &= m_registers[(m_opcode & 0x00F0) >> 4];
							m_programCounter += 2;
							break;
						case 0x0003: // 8XY3: Sets VX to VX xor VY. 
							m_registers[(m_opcode & 0x0F00) >> 8] ^= m_registers[(m_opcode & 0x00F0) >> 4];
							m_programCounter += 2;
							break;
						case 0x0004: // 8XY4: Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't. 

							//Check if Y value is greater than remaining space in X register - 0xFF = 255, if it's over 255 set the carry flag
							if (m_registers[(m_opcode & 0x00F0) >> 4] > 0xFF - m_registers[(m_opcode & 0x0F00) >> 8])
								m_registers[0xF] = 1;
							else
								m_registers[0xF] = 0;

							//Add value of register Y into register X
							m_registers[(m_opcode & 0x0F00) >> 8] += m_registers[(m_opcode & 0x00F0) >> 4];
							m_programCounter += 2;
							break;
						case 0x0005: // 8XY5: VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't. 
							if (m_registers[(m_opcode & 0x0F00) >> 8] < m_registers[(m_opcode & 0x00F0) >> 4])
								m_registers[0xF] = 0; // theres a borrow
							else
								m_registers[0xF] = 1;

							m_registers[(m_opcode & 0x0F00) >> 8] -= m_registers[(m_opcode & 0x00F0) >> 4];
							m_programCounter += 2;
							break;
						case 0x0006: // 8XY6: Stores the least significant bit of VX in VF and then shifts VX to the right by 1.
							m_registers[0xF] = (byte) (m_registers[(m_opcode & 0x0F00) >> 8] & 1);
							m_registers[(m_opcode & 0x0F00) >> 8] >>= 1;
							m_programCounter += 2;
							break;
						case 0x0007: // 8XY7: Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't. 
							if (m_registers[(m_opcode & 0x00F0) >> 4] < m_registers[(m_opcode & 0x0F00) >> 8])
								m_registers[0xF] = 0; // theres a borrow
							else
								m_registers[0xF] = 1;

							m_registers[(m_opcode & 0x0F00) >> 8] = (byte) (m_registers[(m_opcode & 0x00F0) >> 4] - m_registers[(m_opcode & 0x0F00) >> 8]);
							m_programCounter += 2;
							break;
						case 0x000E: // 8XYE: Stores the most significant bit of VX in VF and then shifts VX to the left by 1.[3]
							m_registers[0xF] = (byte)(m_registers[(m_opcode & 0x0F00) >> 8] >> 7);
							m_registers[(m_opcode & 0x0F00) >> 8] <<= 1;
							m_programCounter += 2;
							break;
						default:
							Debug.Assert(true, $"Opcode not defined - Opcode:{m_opcode}");
							break;
					}
					break;
				case 0xA000: // ANNN: Set I to the address NNN
					m_indexRegister = (ushort)(m_opcode & 0x0FFF);
					m_programCounter += 2;
					break;
				case 0xB000: // BNNN: Jumps to the address NNN plus V0. 
					break;
				case 0xC000: // CXNN: Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN. 
					m_registers[(m_opcode & 0x0F00) >> 8] = (byte) (m_random.Next(0,255) & (m_opcode & 0x00FF));
					m_programCounter += 2;
					break;
				case 0xD000: // DXYN: Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels. Each row of 8 pixels is read as bit-coded starting from memory location I; I value doesn’t change after the execution of this instruction. As described above, VF is set to 1 if any screen pixels are flipped from set to unset when the sprite is drawn, and to 0 if that doesn’t happen 
					drawSprite();
					m_programCounter += 2;
					break;
				case 0xE000:
					switch (m_opcode & 0x000F) {
						case 0x000E: // EX9E: Skips the next instruction if the key stored in VX is pressed. (Usually the next instruction is a jump to skip a code block) 
							if (m_keypad[m_registers[(m_opcode & 0x0F00) >> 8]] != 0)
								m_programCounter += 4;
							else
								m_programCounter += 2;
							break;
						case 0x0001: // EXA1: Skips the next instruction if the key stored in VX isn't pressed. (Usually the next instruction is a jump to skip a code block) 
							if (m_keypad[m_registers[(m_opcode & 0x0F00) >> 8]] == 0)
								m_programCounter += 4;
							else
								m_programCounter += 2;
							break;
						default:
							Debug.Assert(true, $"Opcode not defined - Opcode:{m_opcode}");
							break;
					}
					break;
				case 0xF000:
					switch (m_opcode & 0x000F) {
						case 0x0007: // FX07: Sets VX to the value of the delay timer. 
							m_registers[(m_opcode & 0x0F00) >> 8] = m_delay_timer;
							m_programCounter += 2;
							break;
						case 0x000A: // FX0A: A key press is awaited, and then stored in VX. (Blocking Operation. All instruction halted until next key event) 
							break;
						case 0x0008: // FX18: Sets the sound timer to VX. 
							m_sound_timer = m_registers[(m_opcode & 0x0F00) >> 8];
							m_programCounter += 2;
							break;
						case 0x000E: // FX1E: Adds VX to I.[4] 
							//TODO: do we gotta Handle carryflag?
							m_indexRegister += m_registers[(m_opcode & 0x0F00) >> 8];
							m_programCounter += 2;
							break;
						case 0x0009: // FX29: Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font. 
							var sprite = (m_opcode & 0x0F00) >> 8;
							m_indexRegister = (ushort)(m_registers[sprite] * 0x5); // you should multiply by 5 the sprite address, for it to pick the fonts correctly, because they are 4x5 pixels
							m_programCounter += 2;
							break;
						case 0x0003: // FX33: Stores the binary-coded decimal representation of VX, with the most significant of three digits at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2. (In other words, take the decimal representation of VX, place the hundreds digit in memory at location in I, the tens digit at location I+1, and the ones digit at location I+2.) 
							m_memory[m_indexRegister] = (byte)(m_registers[(m_opcode & 0x0F00) >> 8] / 100);
							m_memory[m_indexRegister + 1] = (byte)((m_registers[(m_opcode & 0x0F00) >> 8] / 10) % 10);
							m_memory[m_indexRegister + 2] = (byte)((m_registers[(m_opcode & 0x0F00) >> 8] % 100) % 10);
							m_programCounter += 2;
							break;
						case 0x0005:
							switch (m_opcode & 0x00FF) {
								case 0x0015: // FX15: Sets the delay timer to VX. 
									m_delay_timer = (byte) ((m_opcode & 0x0F00) >> 8);
									m_programCounter += 2;
									break;
								case 0x0055: // FX55: Stores V0 to VX (including VX) in memory starting at address I. The offset from I is increased by 1 for each value written, but I itself is left unmodified.
									break;
								case 0x0065: // FX65: Fills V0 to VX (including VX) with values from memory starting at address I. The offset from I is increased by 1 for each value written, but I itself is left unmodified. 
									var vx = (m_opcode & 0x0F00) >> 8;
									for (int i = 0; i < vx; i++) {
										m_registers[i] = m_memory[m_indexRegister + i];
									}
									m_programCounter += 2;
									break;
								default:
									Debug.Assert(true, $"Opcode not defined - Opcode:{m_opcode}");
									break;
							}
							break;
						default:
							Debug.Assert(true, $"Opcode not defined - Opcode:{m_opcode}");
							break;
					}

					break;
				default:
					Debug.Assert(true, $"Opcode not defined - Opcode:{m_opcode}");
					break;
			}

			//Update timers
			if (m_delay_timer > 0)
				--m_delay_timer;

			if (m_sound_timer > 0) {
				if (m_sound_timer == 1)
					Console.Beep();
				--m_sound_timer;
			}
		}

		private void drawSprite()
		{
			//http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/
			ushort x = m_registers[(m_opcode & 0x0F00) >> 8]; //X-coord
			ushort y = m_registers[(m_opcode & 0x00F0) >> 4]; //Y-Coord
			ushort height = (ushort)(m_opcode & 0x000F); //Height of sprite
			ushort width = 8; //Width is always 8 pixels
			ushort pixel;

			m_registers[0xF] = 0; //Reset register VF (used for collision detection)
			for (int yline = 0; yline < height; yline++) {
				pixel = m_memory[m_indexRegister + yline];
				for (int xline = 0; xline < width; xline++) {
					if ((pixel & (0x80 >> xline)) != 0) //scan one bit at a time to check if pixel is set to 1
					{
						if (m_graphics[(x + xline + ((y + yline) * 64)) % 2048] == 1)
							m_registers[0xF] = 1; //Register the collision

						m_graphics[(x + xline + ((y + yline) * 64)) % 2048] ^= 1;
					}
				}
			}

			m_drawFlag = true;
		}

		public void SetKeys(Keys[] keymap)
		{
			for (int i = 0; i < keymap.Length; i++) {
				var state = Keyboard.GetState();
				if (state.IsKeyDown(keymap[i]))
					m_keypad[i] = 1;
				else
					m_keypad[i] = 0;
			}
		}

	}
}
